import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';

import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useState } from 'react';
import Swal from 'sweetalert2';

import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';

const theme = createTheme();

export default function AddProduct() {
      const accessToken = localStorage.getItem("token");
      const [name, setName] = useState('');
      const [description, setDescription] = useState('');
      const [price, setPrice] = useState('');
      const [id, setId] = useState('');
      const [isActive, setIsActive] = useState(false);

      function addProduct(e) {
            e.preventDefault();

            fetch('http://localhost:4000/products/addProduct', {
                  method: 'POST',
                  headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${accessToken}`
                  },
                  body: JSON.stringify({
                        name: name,
                        description: description,
                        price: price,
                  })
            })
                  .then(res => res.json())
                  .then(data => {
                        console.log(data);

                        if (data.name) {
                              Swal.fire({
                                    title: 'Product Added!',
                                    icon: 'success',
                              });

                        } else {
                              Swal.fire({
                                    title: 'Adding Product failed',
                                    icon: 'error',
                                    text: 'Something went wrong, try again'
                              });
                        };
                  });
            setName('');
            setDescription('');
            setPrice('');
      };

      function editProduct(e) {
            fetch(`http://localhost:4000/products/updateProduct/${id}`, {
                  method: 'PUT',
                  body: JSON.stringify({
                        name: name,
                        description: description,
                        price: price,
                        isActive: isActive
                  }),
                  headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${accessToken}`
                  }
            })
                  .then(response => response.json())
                  .then(data => {
                        console.log(data);
                        alert('Post successfully updated');

                  });
      }

      return (
            <ThemeProvider theme={theme}>
                  <Container component="main" maxWidth="xs" sx={{ pb: 8 }}>
                        <CssBaseline />
                        <Box
                              sx={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    alignItems: 'center',
                              }}
                        >
                              <Typography component="h1" variant="h5">
                                    Add Product
                              </Typography>

                              <Box component="form" onSubmit={e => addProduct(e)}>

                                    <Grid container spacing={0}>
                                          <Grid item xs={12}>

                                                <TextField
                                                      margin="normal"
                                                      required
                                                      fullWidth
                                                      id="name"
                                                      label="Name"
                                                      name="name"
                                                      value={name}
                                                      onChange={e => setName(e.target.value)}
                                                />
                                          </Grid>

                                          <Grid item xs={12}>
                                                <TextField
                                                      margin="normal"
                                                      required
                                                      fullWidth
                                                      id="description"
                                                      label="Description"
                                                      name="descritpion"
                                                      value={description}
                                                      onChange={e => setDescription(e.target.value)}
                                                />
                                          </Grid>

                                          <Grid item xs={12}>
                                                <TextField
                                                      margin="normal"
                                                      required
                                                      fullWidth
                                                      id="price"
                                                      label="Price"
                                                      name="price"
                                                      value={price}
                                                      onChange={e => setPrice(e.target.value)}
                                                />
                                          </Grid>

                                    </Grid>
                                    <Button
                                          type="submit"
                                          fullWidth
                                          variant="contained"
                                          sx={{ mt: 3, mb: 2 }}
                                    >
                                          Add
                                    </Button>
                              </Box>
                        </Box>


                        {/* EDIT PRODUCT */}
                        <Box
                              sx={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    alignItems: 'center',
                                    pt: 8
                              }}
                        >
                              <Typography component="h1" variant="h5">
                                    Update Product
                              </Typography>

                              <Box component="form" onSubmit={e => editProduct(e)}>

                                    <Grid container spacing={0}>
                                          <Grid item xs={12}>

                                                <TextField
                                                      margin="normal"
                                                      required
                                                      fullWidth
                                                      id="id"
                                                      label="ID"
                                                      name="product id"
                                                      onChange={e => setId(e.target.value)}
                                                />
                                          </Grid>

                                          <Grid item xs={12}>

                                                <TextField
                                                      margin="normal"
                                                      required
                                                      fullWidth
                                                      id="name"
                                                      label="Name"
                                                      name="name"
                                                      value={name}
                                                      onChange={e => setName(e.target.value)}
                                                />
                                          </Grid>

                                          <Grid item xs={12}>
                                                <TextField
                                                      margin="normal"
                                                      required
                                                      fullWidth
                                                      id="description"
                                                      label="Description"
                                                      name="descritpion"
                                                      value={description}
                                                      onChange={e => setDescription(e.target.value)}
                                                />
                                          </Grid>

                                          <Grid item xs={12}>
                                                <TextField
                                                      margin="normal"
                                                      required
                                                      fullWidth
                                                      id="price"
                                                      label="Price"
                                                      name="price"
                                                      value={price}
                                                      onChange={e => setPrice(e.target.value)}
                                                />
                                          </Grid>

                                    </Grid>

                                    <FormGroup>
                                          <FormControlLabel control={
                                                <Checkbox
                                                      id = "id"
                                                      defaultChecked={true}
                                                      value ={isActive}
                                                      // onChange={handleChange}
                                                      onChange={e => setIsActive(e.target.value)}
                                                />}
                                                label="Active?"
                                          />
                                    </FormGroup>

                                    <Button
                                          type="submit"
                                          fullWidth
                                          variant="contained"
                                          sx={{ mt: 3, mb: 2 }}
                                    >
                                          Add
                                    </Button>
                              </Box>
                        </Box>
                  </Container>
            </ThemeProvider>
      );
}