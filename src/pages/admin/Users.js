import * as React from 'react';
import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import { useState, useEffect } from 'react'


const columns = [
      { field: '_id', headerName: 'ID', width:300 },
      { field: 'firstName', headerName: 'First Name', width: 250 },
      { field: 'lastName', headerName: 'Last Name', width: 250 },
      { field: 'email', headerName: 'Email', width: 250 },
      { field: 'mobileNo', headerName: 'Mobile Number', width: 250 },
]

    
export default function UserGrid() {
      const accessToken = localStorage.getItem("token");

      const [tableData, setTableData] = useState([]);
      
      console.log(accessToken);

      useEffect(() => {
            fetch("https://shielded-atoll-13453.herokuapp.com/user/users", {
                  headers: {
                        Authorization: `Bearer ${accessToken}`
                    }
            })
                  .then((data) => data.json())
                  .then((data) => setTableData(data))
      }, [accessToken])
      console.log(tableData)

    

      return (
            <Box sx={{
                  height: 800,
                  width: '100%',
                  bgcolor: 'inherit',
                  pt: 8,
                  py: 10
            }}>
                  <DataGrid
                        getRowId={row => row._id}
                        rows={tableData}
                        columns={columns}
                        pageSize={12}
                  />
            </Box>
      );
}
