import * as React from 'react';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
//import Link from '@mui/material/Link';
import Skeleton from '@mui/material/Skeleton';
import Modal from '@mui/material/Modal';

import ProductView from './ProductView';

export default function ProductCard({ productProp }) {
      const [open, setOpen] = React.useState(false);
      const handleOpen = () => setOpen(true);
      const handleClose = () => setOpen(false);

      const { name, description, price, _id } = productProp;

      const info = {
            name: name,
            description: description,
            price: price,
            _id: _id
      }
      // info = React.createRef();



      return (
            <Grid item key={_id} xs={12} sm={6} md={4} lg={3}>
                  <Card
                        sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}
                  >
                        <Skeleton variant="rectangular" sx={{ pt: '56.25%', }} />
                        <CardContent sx={{ flexGrow: 1 }}>
                              <Typography gutterBottom variant="h5" component="h2" sx={{ fontWeight: 500 }}>
                                    {name}
                              </Typography>
                              <Typography>{description}</Typography>
                              <Typography sx={{ fontWeight: 500 }}>PhP {price}</Typography>
                        </CardContent>
                        <CardActions>

                              <Button onClick={handleOpen}>View</Button>
                              <Modal
                                    open={open}
                                    onClose={handleClose}
                              >
                                    <ProductView info={info} />
                              </Modal>

                        </CardActions>
                  </Card>
            </Grid>
      )
}

