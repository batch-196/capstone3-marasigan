import { Box, Button, Container, Typography, Link } from '@mui/material';
import Highlights from '../components/Highlights';
import UserContext from '../UserContext';
import { useContext } from 'react';
import { Navigate } from 'react-router-dom';



export default function Home() {
    const { user } = useContext(UserContext);
    return (
        (user.isAdmin) ?
            <Navigate to="/admin" />
            :
            <>
                <Box
                    component="main"
                    sx={{
                        alignItems: 'center',
                        display: 'flex',
                        flexGrow: 1,
                        minHeight: '100%'
                    }}
                >
                    <Container maxWidth="md">
                        <Box
                            sx={{
                                alignItems: 'center',
                                display: 'flex',
                                flexDirection: 'column',
                                bgcolor: 'inherit',
                                pt: 8,
                                py: 10

                            }}
                        >
                            <Typography
                                align="center"
                                color="textPrimary"
                                variant="h1"
                            >
                                Ecommerce Placeholder
                            </Typography>
                            <Link
                                href="/products"
                                passhrefs
                            >
                                <Button
                                    sx={{ mt: 3 }}
                                    variant="contained"
                                >
                                    Buy
                                </Button>
                            </Link>
                        </Box>
                        <Highlights />
                    </Container>


                </Box>
            </>
    )
}
