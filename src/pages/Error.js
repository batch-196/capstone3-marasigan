import { Box, Button, Container, Typography, Link } from '@mui/material';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';


export default function Error() {


    return (
        <>
            <Box
                component="main"
                sx={{
                    alignItems: 'center',
                    display: 'flex',
                    flexGrow: 1,
                    minHeight: '100%'
                }}
            >
                <Container maxWidth="md">
                    <Box
                        sx={{
                            alignItems: 'center',
                            display: 'flex',
                            flexDirection: 'column',
                            bgcolor: 'inherit',
                            pt: 8,
                            py: 10

                        }}
                    >
                        <Typography
                            align="center"
                            color="textPrimary"
                            variant="h1"
                        >
                            404: Page not found
                        </Typography>
                        <Link
                            href="/"
                        >
                            <Button
                                component="a"
                                startIcon={(<ArrowBackIcon fontSize="small" />)}
                                sx={{ mt: 3 }}
                                variant="contained"
                            >
                                Go back to Home
                            </Button>
                        </Link>
                    </Box>
                </Container>
            </Box>
        </>
    )
}
