import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';

import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useState, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


function Copyright(props) {
      return (
            <Typography variant="body2" color="text.secondary" align="center" {...props}>
                  {'Copyright © '}
                  <Link color="inherit" href="/">
                        Ecomerce
                  </Link>
                  {' 2022.'}
            </Typography>
      );
}

const theme = createTheme();

export default function SignUp() {

      const { user } = useContext(UserContext);
      const history = useNavigate();

      const [firstName, setFirstName] = useState('');
      const [lastName, setLastName] = useState('');
      const [mobileNo, setMobileNo] = useState('');
      const [email, setEmail] = useState('');
      const [password, setPassword] = useState('');

      function registerUser(e) {
            e.preventDefault();

            console.log(email);

            fetch('https://shielded-atoll-13453.herokuapp.com/user/checkUserExists', {
                  method: 'POST',
                  headers: {
                        'Content-Type': 'application/json'
                  },
                  body: JSON.stringify({
                        email: email,
                        mobileNo: mobileNo
                  })
            })
                  .then(res => res.json())
                  .then(data => {
                        console.log(data);

                        if (data) {
                              Swal.fire({
                                    title: "Duplicate User found",
                                    icon: "info",
                                    text: "The email that you're trying to register already exist"
                              });

                        } else {
                              fetch('https://shielded-atoll-13453.herokuapp.com/users/register', {
                                    method: 'POST',
                                    headers: {
                                          'Content-Type': 'application/json'
                                    },
                                    body: JSON.stringify({
                                          firstName: firstName,
                                          lastName: lastName,
                                          email: email,
                                          mobileNo: mobileNo,
                                          password: password
                                    })
                              })
                                    .then(res => res.json())
                                    .then(data => {
                                          console.log(data);

                                          if (data.email) {
                                                Swal.fire({
                                                      title: 'Registration successful!',
                                                      icon: 'success',
                                                      text: 'Thank you for registering'
                                                });
                                                history("/login");

                                          } else {
                                                Swal.fire({
                                                      title: 'Registration failed',
                                                      icon: 'error',
                                                      text: 'Something went wrong, try again'
                                                });
                                          };
                                    });
                        };
                  });
            setEmail('');
            setPassword('');
            setFirstName('');
            setLastName('');
            setMobileNo('');
      };

      return (
            (user.id !== null) ?
                  <Navigate to="/products" />
                  :
                  <ThemeProvider theme={theme}>
                        <Container component="main" maxWidth="xs">
                              <CssBaseline />
                              <Box
                                    sx={{
                                          pt: 8,
                                          py: 10,
                                          marginTop: 8,
                                          display: 'flex',
                                          flexDirection: 'column',
                                          alignItems: 'center',
                                    }}
                              >
                                    <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                                          <LockOutlinedIcon />
                                    </Avatar>
                                    <Typography component="h1" variant="h5">
                                          Sign up
                                    </Typography>

                                    <Box component="form" onSubmit={e => registerUser(e)}>

                                          <Grid container spacing={2}>
                                                <Grid item xs={12} sm={6}>

                                                      <TextField
                                                            margin="normal"
                                                            required
                                                            fullWidth
                                                            id="firstName"
                                                            label="First Name"
                                                            name="firstName"
                                                            value={firstName}
                                                            onChange={e => setFirstName(e.target.value)}
                                                      />
                                                </Grid>
                                                <Grid item xs={12} sm={6}>
                                                      <TextField
                                                            margin="normal"
                                                            required
                                                            fullWidth
                                                            id="lastName"
                                                            label="Last Name"
                                                            name="lastName"
                                                            value={lastName}
                                                            onChange={e => setLastName(e.target.value)}
                                                      />
                                                </Grid>
                                                <Grid item xs={12}>
                                                      <TextField
                                                            margin="normal"
                                                            required
                                                            fullWidth
                                                            id="email"
                                                            label="Email Address"
                                                            name="email"
                                                            autoComplete="email"
                                                            type="email"
                                                            autoFocus
                                                            value={email}
                                                            onChange={(e) => setEmail(e.target.value)}
                                                      />
                                                </Grid>
                                                <Grid item xs={12}>
                                                      <TextField
                                                            margin="normal"
                                                            required
                                                            fullWidth
                                                            id="mobileNo"
                                                            label="Mobile Number"
                                                            name="mobileNo"
                                                            value={mobileNo}
                                                            onChange={e => setMobileNo(e.target.value)}
                                                      />
                                                </Grid>
                                                <Grid item xs={12}>
                                                      <TextField
                                                            margin="normal"
                                                            required
                                                            fullWidth
                                                            label="Password"
                                                            type="password"
                                                            value={password}
                                                            onChange={(e) => setPassword(e.target.value)}
                                                      />
                                                </Grid>
                                          </Grid>
                                          <Button
                                                type="submit"
                                                fullWidth
                                                variant="contained"
                                                sx={{ mt: 3, mb: 2 }}
                                          >
                                                Sign Up
                                          </Button>
                                          <Grid container justifyContent="flex-end">
                                                <Grid item>
                                                      <Link href="/login" variant="body2">
                                                            Already have an account? Sign in
                                                      </Link>
                                                </Grid>
                                          </Grid>
                                    </Box>
                              </Box>
                              <Copyright sx={{ mt: 5 }} />
                        </Container>
                  </ThemeProvider>
      );
}