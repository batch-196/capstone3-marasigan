import * as React from 'react';
import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import { useState, useEffect } from 'react'
import EditProducts from './components/EditProducts';


const columns = [
      { field: '_id', headerName: 'ID', width: 250 },
      { field: 'name', headerName: 'Name', width: 250 },
      { field: 'description', headerName: 'Description', width: 250 },
      { field: 'price', headerName: 'Price (PhP)', width: 100 },
      { field: 'createdOn', headerName: 'Date Added', width: 200 },
      { field: 'isActive', headerName: 'Active', width: 250, type:'boolean' },
]


export default function UserGrid() {
      const accessToken = localStorage.getItem("token");
      const [tableData, setTableData] = useState([]);
      useEffect(() => {
            fetch("https://shielded-atoll-13453.herokuapp.com/products/", {
                  headers: {
                        Authorization: `Bearer ${accessToken}`
                  }
            })
                  .then((data) => data.json())
                  .then((data) => setTableData(data))
      }, [accessToken])



      return (
            <>
                  <Box sx={{
                        height: 800,
                        width: '100%',
                        bgcolor: 'inherit',
                        pt: 8,
                        py: 10
                  }}>
                        <DataGrid
                              editMode="row"
                              getRowId={row => row._id}
                              rows={tableData}
                              columns={columns}
                              pageSize={12}
                        />
                  </Box>
                  <EditProducts />
            </>
      );
}
