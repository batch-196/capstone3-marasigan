import { Box, Button, Container, Typography, Link } from '@mui/material';
import UserContext from '../../UserContext';
import { useContext } from 'react';
import { Navigate } from 'react-router-dom';

export default function Admin() {
      const { user } = useContext(UserContext);
      return (
            (user.isAdmin === false) ?
                  <Navigate to="/" />
                  :
                  <>
                        <Box
                              component="main"
                              sx={{
                                    alignItems: 'center',
                                    display: 'flex',
                                    flexGrow: 1,
                                    minHeight: '100%'
                              }}
                        >
                              <Container maxWidth="md">
                                    <Box
                                          sx={{
                                                alignItems: 'center',
                                                display: 'flex',
                                                flexDirection: 'column',
                                                bgcolor: 'inherit',
                                                pt: 8,
                                                py: 10

                                          }}
                                    >
                                          <Typography
                                                align="center"
                                                color="textPrimary"
                                                variant="h1"
                                          >
                                                Admin Home
                                          </Typography>

                                    </Box>
                              </Container>
                        </Box>
                  </>
      )
}
