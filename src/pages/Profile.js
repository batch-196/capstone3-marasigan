import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { useState, useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';

function Copyright(props) {

      return (
            <Typography variant="body2" color="text.secondary" align="center" {...props}>
                  {'Copyright © '}
                  <Link color="inherit" href="/">
                        Ecomerce
                  </Link>
                  {' 2022.'}
            </Typography>
      );
}

const theme = createTheme();

export default function Login() {
      const accessToken = localStorage.getItem("token");

      const { user, setUser } = useContext(UserContext);

      useEffect(() => {
            fetch('https://shielded-atoll-13453.herokuapp.com/user/getUserDetails', {
                  headers: {
                        Authorization: `Bearer ${accessToken}`
                  }
            })
                  .then(res => res.json())
                  .then(data => {
                        setUser({
                              firstName: data.firstName,
                              lastName : data.lastName,
                              email: data.email,
                              mobileNo: data.mobileNo
                          });
                  })
      }, [])

      return (

            <ThemeProvider theme={theme}>
                  <Container component="main" maxWidth="sm">
                        <CssBaseline />
                        <Box
                              sx={{
                                    pt: 8,
                                    py: 10,
                                    marginTop: 8,
                                    display: 'flex',
                                    flexDirection: 'column',
                                    alignItems: 'center',
                              }}
                        >

                              <Typography variant="h4">
                                    Profile
                              </Typography>

                              <Box sx = {{pt: 8,
                                    py: 10,}}>

                                    <Typography component="h4" variant="h5">
                                          First Name: {user.firstName}
                                    </Typography>
                                    <Typography component="h4" variant="h5">
                                          Last Name: {user.lastName}
                                    </Typography>
                                    <Typography component="h4" variant="h5">
                                          Mobile Number: {user.mobileNo}
                                    </Typography>
                                    <Typography component="h4" variant="h5">
                                          Email Address: {user.email}
                                    </Typography>

                              </Box>
                        </Box>
                        <Copyright sx={{ mt: 8, mb: 4 }} />
                  </Container>
            </ThemeProvider>
      );
}