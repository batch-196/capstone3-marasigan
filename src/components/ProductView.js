import {useContext } from 'react';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import './style.css';


import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Paper from '@mui/material/Paper';
import * as React from 'react';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';

export default function ProductView({ info }) {

      const { user } = useContext(UserContext);

      const history = useNavigate();

      const { name, description, price, _id } = info;


      const { productId } = useParams();

      const addOrder = () => {
            fetch('https://shielded-atoll-13453.herokuapp.com/orders/addOrder', {
                  method: 'POST',
                  headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                  },
                  body: JSON.stringify({
                        totalAmount: price,
                        products: [
                              {
                                    productId: _id,
                                    quantity: 1
                              }
                        ]
                  })
            })
                  .then(res => res.json())
                  .then(data => {
                        console.log(data);

                        if (data) {
                              Swal.fire({
                                    title: 'Successfully Ordered!',
                                    icon: 'success',
                                    text: 'Thank you for Ordering',
                                    customClass: {
                                          container: 'my-swal'
                                    }
                              });

                              history("/products");

                        } else {
                              Swal.fire({
                                    title: 'Something went wrong',
                                    icon: 'error',
                                    text: 'Please try again later'
                              });
                        };

                  });
      };


      return (

            <Box sx={{
                  position: 'absolute',
                  top: '50%',
                  left: '50%',
                  transform: 'translate(-50%, -50%)',
                  minWidth: '75%',
                  bgcolor: 'background.paper',
            }}>
                  <Grid container component="main" spacing={0}

                        sx={{
                              height: '50vh'
                        }}>

                        <CssBaseline />

                        <Grid
                              item
                              xs={false}
                              sm={6}
                              md={6}
                              sx={{
                                    backgroundImage: 'url(http://via.placeholder.com/900)',
                                    backgroundRepeat: 'no-repeat',
                                    backgroundColor: (t) =>
                                          t.palette.mode === 'light' ? t.palette.grey[50] : t.palette.grey[900],
                                    backgroundSize: 'cover',
                                    backgroundPosition: 'center',

                              }}
                        />
                        <Grid item component={Paper} elevation={1} square
                              sx={{}}>

                              <Box
                                    sm={8}
                                    md={8}
                                    sx={{
                                          backgroundSize: 'cover',

                                    }}
                              >
                                    <Typography component="h1" variant="h5" align="center">
                                          {name}
                                    </Typography>
                                    <Typography component="h6" variant="h6" sx={{ fontWeight: 300 }}>
                                          {description}
                                    </Typography>
                                    <Typography component="h6" variant="h6" sx={{ fontWeight: 500 }}>
                                          PhP {price}
                                    </Typography>
                                    <Grid container>

                                          {user.id !== null ?
                                                <Button variant="primary" onClick={() => addOrder(productId)}>Order</Button>
                                                :
                                                <Link className="btn btn-danger" to="/login">Log In</Link>
                                          }

                                    </Grid>
                              </Box>
                        </Grid>
                  </Grid>
            </Box>

            // <Container className="mt-5">
            //       <Row>
            //             <Col lg={{ span: 6, offset: 3 }}>
            //                   <Card>
            //                         <Card.Body>
            //                               <Card.Title>{name}</Card.Title>
            //                               <Card.Subtitle>Description:</Card.Subtitle>
            //                               <Card.Text>{description}</Card.Text>
            //                               <Card.Subtitle>Price:</Card.Subtitle>
            //                               <Card.Text>Php {price}</Card.Text>
            //                               <Card.Subtitle>Class Schedule</Card.Subtitle>
            //                               <Card.Text>8:00 AM to 5:00 PM</Card.Text>
            //                               {user.id !== null ?
            //                                     <Button variant="primary" onClick={() => enroll(courseId)}>Enroll</Button>
            //                                     :
            //                                     <Link className="btn btn-danger" to="/login">Log In</Link>
            //                               }
            //                         </Card.Body>
            //                   </Card>
            //             </Col>
            //       </Row>
            // </Container>
      )
}

