import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Link from '@mui/material/Link';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import ProductCard from '../components/ProductCards';
import { useState, useEffect } from 'react';

function Copyright(props) {
      return (
            <Typography variant="body2" color="text.secondary" align="center" {...props}>
                  {'Copyright © '}
                  <Link color="inherit" href="/">
                        Ecomerce
                  </Link>
                  {' 2022.'}
            </Typography>
      );
}

const theme = createTheme();

export default function Products() {
      const [products, setProducts] = useState([]);
      useEffect(() => {
            fetch("https://shielded-atoll-13453.herokuapp.com/products")
                  .then(res => res.json())
                  .then(data => {
                        console.log(data);
                        setProducts(data.map(product => {
                              return (
                                    <ProductCard key={product._id} productProp={product} />
                              )
                        }));

                  });

      }, []);

      return (
            <ThemeProvider theme={theme}>
                  <CssBaseline />
                  <main>
                        <Box
                              sx={{
                                    bgcolor: 'inherit',
                                    pt: 8,
                                    py: 10
                              }}
                        >
                              <Container maxWidth="lg">
                                    <Typography
                                          component="h1"
                                          variant="h2"
                                          align="center"
                                          color="text.primary"
                                          gutterBottom
                                    >
                                          Products
                                    </Typography>
                              </Container>
                        </Box>

                        <Container sx={{ py: 4 }} maxWidth="xl">
                              <Grid container spacing={4}>
                                    {products}
                              </Grid>
                        </Container>
                  </main>

                  <Box sx={{p: 6 }} component="footer">
                        <Typography variant="h6" align="center" gutterBottom>
                              Footer
                        </Typography>
                        <Copyright />
                  </Box>
            </ThemeProvider>
      );
}